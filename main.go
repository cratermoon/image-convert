package main

import (
	"fmt"
	"image"
	"image/png"
	_ "image/jpeg"
	"log"
	"os"
	"path/filepath"
	"strings"

	_ "golang.org/x/image/webp"
)

func failOnError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
func main() {
	if len(os.Args) < 2 {
		log.Fatalf("usage: %s <image filename>", os.Args[0])
	}
	in := os.Args[1]
	o := strings.TrimSuffix(filepath.Base(in), filepath.Ext(in)) + ".png"
	fmt.Printf("converting %s to %s\n", in, o)

	inf, err := os.Open(in)
	failOnError(err)
	defer inf.Close()
	img, _, err := image.Decode(inf)
	failOnError(err)
	fmt.Println(img.Bounds())	
	pngf, err := os.Create(o)
	failOnError(err)
	defer pngf.Close()
	if err := png.Encode(pngf, img); err != nil {
		log.Fatal(err)
	}
}
