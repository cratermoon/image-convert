![Go gopher](gopher.png)

# Image Converter in Go

Mostly just exploring the available [Go programming language](https://go.dev/) tools and [libraries for image data manipulation](https://go.dev/blog/image).

The Go gopher was designed by [Renee French](https://reneefrench.blogspot.com/). Used under the Creative Commons 3.0 Attributions license.
